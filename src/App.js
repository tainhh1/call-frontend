import { Route, Routes } from 'react-router-dom';
import Sidebar from './components/layouts/Sidebar';
import CallHistory from './pages/CallHistory';
import DialNumber from './pages/DialNumber';

function App() {
	return (
		<div className='container-fluid'>
			<div className='row'>
				<div className='col-md-2 col-sm-12'>
					<Sidebar />
				</div>
				<div className='col-md-10 col-sm-12'>
					<Routes>
						<Route path='/' element={<DialNumber />}></Route>
						<Route path='/log' element={<CallHistory />}></Route>
					</Routes>
				</div>
			</div>
		</div>
	);
}

export default App;
