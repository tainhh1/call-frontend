import { Fragment, useContext } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { socketContext } from '../../store/SocketProvider';
import DeleteBtn from '../UI/DeleteBtn';
import './HistoryItem.css';

const HistoryItem = (props) => {
	const socket = useContext(socketContext);

	const handleDeleteHistory = () => {
		socket.emit('deleteLogById', props.id);
		props.deleteChanged();
		toast.error('Deleted log');
	};

	return (
		<Fragment>
			<div className='history__item'>
				<div>
					<h2 className='history__item-recipient'>{props.recipient}</h2>
					<h3 className='history__item-status'>{props.status}</h3>
				</div>
				<DeleteBtn onClick={handleDeleteHistory} />
			</div>
			<ToastContainer
				position='top-right'
				autoClose={2000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				draggable
			/>
		</Fragment>
	);
};

export default HistoryItem;
