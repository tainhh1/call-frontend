import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { setPhoneNumber, removeNumberOfPhone } from '../../store/phoneSlice';
import './Call.css';
import NumPad from '../NumPad/NumPad';

const Call = (props) => {
	const dispatch = useDispatch();
	const enteredValue = useSelector((state) =>
		state.phoneReducer.phoneNumber.trim(),
	);

	const changePhoneNumberHandler = (event) => {
		dispatch(setPhoneNumber(event.target.value));
	};

	const removePhoneNumberClickHandler = () => {
		dispatch(removeNumberOfPhone());
	};
	return (
		<div>
			<div className='dial__phone-number'>
				<input
					maxLength={10}
					onChange={changePhoneNumberHandler}
					value={enteredValue}
					type='text'
				/>
				<i
					onClick={removePhoneNumberClickHandler}
					className='fas fa-backspace'></i>
			</div>
			<div className='dial__key-pad'>
				<NumPad />
			</div>
		</div>
	);
};

export default Call;
