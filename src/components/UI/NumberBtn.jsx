import { useDispatch } from 'react-redux';
import { addNumberToPhone } from '../../store/phoneSlice';
import './NumberBtn.css';

const NumberBtn = (props) => {
	const dispatch = useDispatch();

	const clickNumberHandler = () => {
		dispatch(addNumberToPhone(props.value));
	};

	return (
		<div className='key-pad__btn'>
			<button onClick={clickNumberHandler}>{props.children}</button>
		</div>
	);
};

export default NumberBtn;
