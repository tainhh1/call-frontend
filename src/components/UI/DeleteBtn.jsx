import './DeleteBtn.css';

const DeleteBtn = (props) => {
	return (
		<button onClick={props.onClick} className='history__btn'>
			<i className='fas fa-times'></i>
		</button>
	);
};

export default DeleteBtn;
