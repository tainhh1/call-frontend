export let time;

export const timerCount = (count) => {
	return (dispatch) => {
		time = setInterval(() => {
			dispatch(count());
		}, 1000);
	};
};
